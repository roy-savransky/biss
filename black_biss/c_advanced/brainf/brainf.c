#include <stdio.h>
#include <string.h>
#include <malloc.h>

#define size 1000

int count_dots(char* str)
{
	int counter = 0;
	int data[size];
	for (int i = 0; i < size; i++)
	{
		data[i] = 0;
	}
	int index = 0;
	for (int i = 0; i <= strlen(str); i++)
	{
		switch (str[i])
		{
		case '+':
			data[index]++;
			if (data[index] > 255)
				data[index] -= 256;
			break;
		case '-':
			data[index]--;
			if (data[index] < 0)
				data[index] += 256;
			break;
		case '>':
			if (index == size)
				index = 0;
			else
				index++;
			break;
		case '<':
			if (index == 0)
				index = 999;
			else
				index--;
			break;
		case '.':
			counter++;
			break;
		case ']':
			if (data[index] != 0)
			{
				while (str[i] != '[')
					i--;
			}
			break;
		case '[':
			if (data[index] == 0)
			{
				while (str[i] != ']')
					i++;
			}
			break;
		default:
			break;
		}
	}
	return counter;
}

char* interapter(char* str)
{
	
	char* print = (char*)malloc(sizeof(char)*(count_dots(str)+1));
	int pos_p = 0;
	int data[size];
	for (int i = 0; i < size; i++)
	{
		data[i] = 0;
	}
	int index = 0;
	for (int i = 0; i <= strlen(str); i++)
	{

		printf("%d , %c, %d\n", i, str[i], data[index] );
		switch (str[i])
		{
		case '+':
			data[index]++;
			if (data[index] > 255)
				data[index] -= 256;
			break;
		case '-':
			data[index]--;
			if (data[index] < 0)
				data[index] += 256;
			break;
		case '>':
			if (index == size)
				index = 0;
			else
				index++;
			break;
		case '<':
			if (index == 0)
				index = 999;
			else
				index--;
			break;
		case '.':
			print[pos_p] = (char)data[index];
			pos_p++;
			break;
		case ']':
			if (data[index] != 0)
			{
				while (str[i] != '[')
					i--;
			}
			break;
		case '[':
			if (data[index] == 0)
			{
				while (str[i] != ']')
					i++;
			}
			break;
		default:
			break;
		}
	}
	print[pos_p] = '\0';
	return print;
}


int main()
{
	char* str = "++++[++++>---<]>-.-[--->+<]>--.++++++++++..+[---->+<]>+++.[-->+++<]>+.-.[--->++<]>+.";
	char* code = interapter(str);
	printf("%s\n", code);
	return 0;
}