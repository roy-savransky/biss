#include <stdio.h>
#include <malloc.h>
typedef struct rectangle {
	
	int length;
	int width;
} rec;

typedef struct linked_list {
	rec value;
	struct linked_list* next;
} node;

node* rec2sqr(rec r1)
{
	node* head = (node*)malloc(sizeof(node));
	
	if (r1.length == r1.width)
	{
		
		head->value = r1;
		head->next = NULL;
		
	}
	
	else if (r1.length > r1.width)
	{
		rec r2 = { r1.width, r1.width };
		rec r3 = { r1.length - r1.width, r1.width };

		head->next = rec2sqr(r3);
		head->value = r2;
	}

	else
	{
		rec r2 = { r1.length, r1.length };
		rec r3 = { r1.length , r1.width - r1.length };

		head->next = rec2sqr(r3);
		head->value = r2;
	}

	return head;
}

void print(rec r)
{
	printf("%dX%d\n", r.length, r.width);
}

void print_list(node* l)
{
	while (l != NULL)
	{
		print(l->value);
		l = l->next;
	}
}

int main()
{

	rec r1 = { 3,10 };
	print_list(rec2sqr(r1));
	return 0;
}