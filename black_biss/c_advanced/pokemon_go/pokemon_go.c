#include <stdio.h>

typedef struct pokemon
{
	float attack;
	float defence;
	char* effectivness;
	char* name;
} pokemon;

void pokemon_buttle(pokemon p1, pokemon p2)
{
	float strength1=0, strength2=0;
	if (p1.effectivness == p2.effectivness)
	{
		strength1 = 50 * 2 * p1.attack / p2.defence;
		strength2 = 50 * 2 * p2.attack / p1.defence;
	}
	else
	{
		if (p1.effectivness == "f")
		{
			if (p2.effectivness == "a")
			{
				strength1 = 50 * 3 * p1.attack / p2.defence;
				strength2 = 50 * 1 * p2.attack / p1.defence;
			}
			else if (p2.effectivness == "e")
			{
				strength1 = 50 * 2 * p1.attack / p2.defence;
				strength2 = 50 * 2 * p2.attack / p1.defence;
			}
			else if (p2.effectivness == "w")
			{
				strength1 = 50 * 1 * p1.attack / p2.defence;
				strength2 = 50 * 3 * p2.attack / p1.defence;
			}
		}
		else if(p1.effectivness == "a")
		{
			if (p2.effectivness == "f")
			{
				strength1 = 50 * 1 * p1.attack / p2.defence;
				strength2 = 50 * 3 * p2.attack / p1.defence;
			}
			else if (p2.effectivness == "e")
			{
				strength1 = 50 * 3 * p1.attack / p2.defence;
				strength2 = 50 * 1 * p2.attack / p1.defence;
			}
			else if (p2.effectivness == "w")
			{
				strength1 = 50 * 2 * p1.attack / p2.defence;
				strength2 = 50 * 2 * p2.attack / p1.defence;
			}
		}
		else if (p1.effectivness == "w")
		{
			if (p2.effectivness == "a")
			{
				strength1 = 50 * 2 * p1.attack / p2.defence;
				strength2 = 50 * 2 * p2.attack / p1.defence;
			}
			else if (p2.effectivness == "e")
			{
				strength1 = 50 * 1 * p1.attack / p2.defence;
				strength2 = 50 * 3 * p2.attack / p1.defence;
			}
			else if (p2.effectivness == "f")
			{
				strength1 = 50 * 3 * p1.attack / p2.defence;
				strength2 = 50 * 1 * p2.attack / p1.defence;
			}
		}
		else if (p1.effectivness == "e")
		{
			if (p2.effectivness == "a")
			{
				strength1 = 50 * 1 * p1.attack / p2.defence;
				strength2 = 50 * 3 * p2.attack / p1.defence;
			}
			else if (p2.effectivness == "f")
			{
				strength1 = 50 * 2 * p1.attack / p2.defence;
				strength2 = 50 * 2 * p2.attack / p1.defence;
			}
			else if (p2.effectivness == "w")
			{
				strength1 = 50 * 3 * p1.attack / p2.defence;
				strength2 = 50 * 1 * p2.attack / p1.defence;
			}
		}
	}

	if (strength1 > strength2)
	{
		printf("%s : %f >> %s : %f\n", p1.name, strength1, p2.name, strength2);
	}
	else if (strength1 < strength2)
	{
		printf("%s : %f >> %s : %f\n", p2.name, strength2, p1.name, strength1);
	}
	else
		printf("tie!\n% s : % f >> % s : % f", p2.name, strength2, p1.name, strength1);
}


int main()
{
	pokemon p1 = { 20, 5, "w", "roy" };
	pokemon p2 = { 10, 5, "e", "elad" };

	pokemon_buttle(p1, p2);
	return 0;
}