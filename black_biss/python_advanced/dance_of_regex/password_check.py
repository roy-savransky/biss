"""
Black Biss - Advance Python

Write a function that read one line from the user with many potentional passwords seperated by comma ","
and print out only the valid password - by the following rules:
    * contain letter from each of the characters-sets:
        lower-case english, upper-case english, decimal digit,
        special characters *&@#$%^
    * dosn't contain other symbols (ignore white-spaces)
    * length between 6 and 12

"""


def validate_passwords():
    up, low, num, special = False,False,False,False
    options = (input("enter optional passwords: \n")).split(',')
    valid = []
    for option in options:
        option = option.strip()
        up, low, num, special = False,False,False, False
        if len(option) >=6 and len(option) <= 12:
            for char in option:
                if char>'a' and char<'z':
                    low = True
                elif char>'A' and char<'Z':
                    up = True
                elif char>'0' and char<'9':
                    num = True
                elif char in ('*', '&', '@', '#', '$', '%', '^'):
                    special = True
        if up and low and num and special:
            valid.append(option)
    print (valid)


# small test to check it's work.
if __name__ == '__main__':
    validate_passwords()
