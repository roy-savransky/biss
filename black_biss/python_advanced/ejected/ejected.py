"""
Black Biss - Advance Python

Write a function that calculate how far is our ejected pilot.
Run this program and insert the following lines (each end with Enter):

UP 5
DOWN 3
LEFT 3
RIGHT 2
0

and the result should be 2
"""


def ejected():
    x, y = 0,0
    str1 = input('enter diraction and number of moves: ')
    while str1 != "0":
        command = str1.split(' ')
        print (command)
        if command[0].upper() == "UP":
            y += int(command[1])
        elif command[0].upper() == "DOWN":
            y -= int(command[1])
        elif command[0].upper() == "LEFT":
            x -= int(command[1])
        elif command[0].upper() == "RIGHT":
            x += int(command[1])
        print (x,y)
        str1 = input('enter diraction and number of moves: ')

    distance = (x**2 + y**2)**0.5
    if distance %1 != 0:
        if distance - int(distance) > 0.5:
            distance = int(distance) +1
        else:
            distance = int(distance)
    return distance

# small test to check it's work.
if __name__ == '__main__':
    print(ejected())
