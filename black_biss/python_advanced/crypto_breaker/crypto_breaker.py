"""
Black Biss - Advance Python

Write a function that get a path to file that encrypted with 3 english-small-letter xor key, and print the
deciphered text.
"""


def decrypt(numbers, key):
    decrypt = []
    for num in numbers:
        decrypt.append(chr(num^key))
    return decrypt


def crypto_breaker(file_path):
    file = open(file_path, "r")
    txt = file.read()
    code_numbers = txt.split(",")
    first = ""
    for char in code_numbers[0]:
        if char >= "0" and char<= "9":
            first+=char
    code_numbers[0] = first
    for i in range(len(code_numbers)):
        code_numbers[i] = int(code_numbers[i])
    crypt_1 = code_numbers[::3]
    crypt_2 = code_numbers[1::3]
    crypt_3 = code_numbers[2::3]
    options_for_first = []
    options_for_second = []
    options_for_therd = []
    can = True
    for i in range(97,123):
        for num in crypt_1:
            #print ("i: %s , num: %s" % (i, num))
            
            if num^i < 32 or num^i>126:
                if num^i == 10:
                    pass
                else:
                    #print (i, num, num^i)
                    can = False
        if can == True:
            options_for_first.append(i)
        can = True
        for num in crypt_2:
            #print ("i: %s , num: %s" % (i, num))
            
            if num^i < 32 or num^i>126:
                if num^i == 10:
                    pass
                else:
                    #print (i, num, num^i)
                    can = False
        if can == True:
            options_for_second.append(i)
        can = True
        for num in crypt_3:
            #print ("i: %s , num: %s" % (i, num))
            
            if num^i < 32 or num^i>126:
                if num^i == 10:
                    pass
                else:
                    #print (i, num, num^i)
                    can = False
        if can == True:
            options_for_therd.append(i)
        can = True

    message = ""
    d_1 = decrypt(crypt_1, options_for_first[0])
    d_2 = decrypt(crypt_2, options_for_second[0])
    d_3 = decrypt(crypt_3, options_for_therd[0])
    for i in range(len(d_1)):
       message += d_1[i] + d_2[i] + d_3[i]
    
    print(message)
    


# small test to check it's work.
if __name__ == '__main__':
    crypto_breaker(r'C:\Users\office\Desktop\matzov\roy-savransky-proj\big_secret.txt')
