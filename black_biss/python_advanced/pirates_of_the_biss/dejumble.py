"""
Black Biss - Advance Python

Write a function that suggest which word is the Pirates meant to write.
"""


def counting_letters_dict(word):
    letter_dict = {}
    for char in word:
        if char in letter_dict.keys():
            letter_dict[char] = letter_dict[char] +1
        else:
            letter_dict[char] = 1
    return letter_dict

def compare_dict(dict1, dict2):
    if len(dict1) != len(dict2):
        return False
    for key in dict1:
        if key not in dict2.keys():
            return False
        else:
            if dict1[key] != dict2[key]:
                return False
    return True
def dejumble(word, potentional_words):
    options = []
    for item in potentional_words:
        if compare_dict(counting_letters_dict(word),counting_letters_dict(item)):
            options.append(item)
    return options



# small test to check it's work.
if __name__ == '__main__':

    ret = dejumble("orspt", ["sport", "parrot", "ports", "matey"])
    if len(ret) == 2 and set(ret) == set(["sport", "ports"]):
        print("HAZA! simple test pass")
    else:
        print("Oops, it's not working yet")
