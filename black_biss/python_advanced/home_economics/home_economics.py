"""
Black Biss - Advance Python

Write a function that calculate how many month one should save money to buy new car.
"""


def home_economics(startPriceOld, startPriceNew, savingPerMonth, percentLossByMonth):
    month_count = 0
    amount = 0
    old_car_price = startPriceOld
    new_car_price = startPriceNew
    income = savingPerMonth
    percent = percentLossByMonth
    while amount < new_car_price:
        if month_count %2 == 1:
            percent += 0.5
        amount = old_car_price + income*month_count
        if amount >= new_car_price:
            break
        old_car_price = old_car_price*(1-(percent/100))
        new_car_price = new_car_price*(1-(percent/100))
        month_count+=1
    return month_count, round(amount-new_car_price)
    




# small test to check it's work.
if __name__ == '__main__':

    ret = home_economics(2000, 8000, 1000, 1.5)
    if ret[0] == 6 and ret[1] == 766:
        print("HAZA! simple test pass")
    else:
        print("Oops, it's not working yet")
