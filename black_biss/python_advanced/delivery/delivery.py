"""
Black Biss - Advance Python

Write a function that get array of string, each represent a row in 2-dim map, when:
    'X' is start / destination point
    '-' is road right-left
    '|' is road up-down
    '+' is a turn in the road, can be any turn (which isn't continue straight)
    path = ["           ",
            "X-----+    "
            "      |    "
            "      +---X",
            "           "]

    # Note: this grid is only valid when starting on the right-hand X, but still considered valid
    path = ["                      ",
            "   +-------+          ",
            "   |      +++---+     ",
            "X--+      +-+   X     "]
    nevigate_validate(path)  # ---> True
"""

def can_go(path, x, y, direction):
    if direction == "left":
        return x> 0 and path[y][x-1] in ('-', '+', 'X')
    if direction == "right":
        return x<len(path[y])-1 and path[y][x+1] in ('-', '+', 'X')
    if direction == "up":
        return y>0 and path[y-1][x] in ('|', '+', 'X')
    if direction == "down":
        return y< len(path)-1 and path[y+1][x] in ('|', '+', 'X')


#the function gets the path and the location 
def is_valid(path, row, col, direction):
    x, y = col, row
    print (direction)
    print(x,y)
    if path[y][x] == 'X':
        return True
    right, left, up, down = can_go(path, x, y,"right"),can_go(path, x, y,"left"),can_go(path, x, y,"up"),can_go(path, x, y,"down")
    print ("right : %s"%right)
    print ("left : %s"%left)
    print ("up : %s"%up)
    print ("down : %s"%down)
    if path[y][x] != '+':
        if direction == "left":
            return left and is_valid(path,y, x-1 ,direction)
        elif direction == "right":
            return right and is_valid(path,y, x+1, direction)
        elif direction == "up":
            return up and is_valid(path,  y-1,x, direction)
        elif direction == "down":
            return down and is_valid(path, y+1,x, direction)
        return False
    else:
        if direction == "right" or direction == "left":
            if up and down:
                return False
            elif up:
                return is_valid(path, y-1,x,"up")
            elif down:
                return is_valid(path, y+1,x,"down")
        elif direction =="up" or direction == "down":
            if left and right:
                return False
            elif right:
                return is_valid(path, y,x+1,"right")
            elif left:
                return is_valid(path,  y, x-1,"left")
                
        
def can_one_way(path, x, y):
    right, left, up, down = can_go(path, x, y,"right"),can_go(path, x, y,"left"),can_go(path, x, y,"up"),can_go(path, x, y,"down")
    if up and not left and not down and not right:
        print ("up")
        return True
    elif not up and left and not down and not right:
        print ("left")
        return True
    elif not up and not left and down and not right:
        print ("down")
        return True
    elif not up and not left and not down and  right:
        print ("right")
        return True
    return False

def nevigate_validate(path):
    for i in range(0,len(path)):
        for j in range(0,len(path[i])):
            if path[i][j] == 'X':
                if not can_one_way(path, j,i):
                    return False
                else:
                    if can_go(path, j, i,"right") and is_valid(path, i,j+1,"right"):
                        return True
                    if can_go(path, j, i,"left") and is_valid(path, i,j-1,"left"):
                        return True
                    if can_go(path, j, i,"up") and is_valid(path,i-1, j,"up"):
                        return True
                    if can_go(path, j, i,"down")and is_valid(path, i+1,j,"down"):
                        return True
    return False


# small test to check it's work.
if __name__ == '__main__':
    path = ["           ",
            "X-----+    ",
            "      |    ",
            "      +---X",
            "           "]
    valid = nevigate_validate(path)

    path = ["           ",
            "X--|--+    ",
            "      -    ",
            "      +---X",
            "           "]
    invalid = nevigate_validate(path)
    if valid and not invalid:
        print("HAZA! simple test pass")
    else:
        print("Oops, it's not working yet")
