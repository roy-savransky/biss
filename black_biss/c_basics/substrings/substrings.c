/**
 * @file substrings.c
 * @version 1.0
 * @author ????????
 *
 * @brief program that check if one string is substring of the other and return pointer
 * to the start of the substring in the string.
 *
 * @section LICENSE
 * This program is part of the Black-Biss;
 * ANY USE OF THE PROGRAM OR THE SOURCE CODE WITHOUT THE BLACK-BISS AGREEMENT IS FORBIDDEN.
 *
 */

 // ------------------------------ includes ------------------------------
#include <stdio.h>
#include <stdlib.h>

// ------------------------------ functions -----------------------------
char* my_strstr(const char* a, const char* b)
{
	int len_a = len(a);
	int len_b = len(b);
	int true = 1;
	if (len_a == len_b)
	{
		for (int i = 0; i < len_a; i++)
		{
			if (a[i] != b[i])
				return "";

		}
		return a;
	}
	else if (len_a > len_b)
	{
		for (int i = 0; i < len_a; i++)
		{
			if (a[i] == b[0] && len_a-i >=len_b)
			{
				true = 1;
				for (int j = 1; j < len_b-1 && true ; j++)
				{
					if (b[j] != a[i+j])
					{
						true = 0;
					}
				}

				if (true == 1)
					return b;
			}
		}
		return "";
	}
	else
	{
		for (int i = 0; i < len_b; i++)
		{
			if (b[i] == a[0] && len_b - i >= len_a)
			{
				true = 1;
				for (int j = 1; j < len_a-1 && true; j++)
				{
					if (a[j] != b[i + j])
					{
						true = 0;
					}
				}

				if (true == 1)
					return b;
			}
		}
		return "";
	}

	return NULL;
}

int len(char* a)
{
	int i = 0;
	while (a[i] != '\0')
	{
		i++;
	}
	return i + 1;
}

int main(int argc, char* argv[])
{
	char s1[] = "AAAAAAACCCAAAAAAAA";
	char s2[] = "CCC";

	char* ret = my_strstr(s1, s2);

	printf("The substring is: %s\n", ret);

	return 0;
}