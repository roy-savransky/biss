#include <stdio.h>
#include <malloc.h>
typedef struct list
{
	int value;
	struct list* next;
} node;

node* add_first(node* head, int Value);
int print(node* head)
{
	node* position;
	position = head;
	while (position != NULL)
	{
		printf("%d -> ", position->value);
		position = position->next;
	}
	printf("\n");
	return 0;
}

node* add_at(node* head, int Value, int N)
{
	int counter = 0;
	node* n = (node*)malloc(sizeof(node));
	n->value = Value;
	n->next = NULL;
	node* p = head;
	if (N == 0)
	{
		head = add_first(head, Value);
		return head;
	}

	while (counter < N - 1 && p->next!=NULL)
	{
		p = p->next;
	}
	if (p->next == NULL)
	{
		p->next = n;
		return head;
	}
	n->next = p->next;
	p->next = n;

	return head;
}

int list_size(node* head)
{
	int counter = 0;
	node* p = head;
	while (p != NULL)
	{
		counter++;
		p = p->next;
	}

	return counter;
}

node* add_first(node* head, int Value)
{
	node* n = NULL;
	n = (node*)malloc(sizeof(node));
	n->value = Value;
	n->next = head;
	head = n;
	return head;
}
int main()
{
	node* first = (node*)malloc(sizeof(node));
	first->value = 3;
	first->next = NULL;
	first = add_first(first, 4);
	first = add_at(first, 1, 1);
	print(first);


	return 0;
}