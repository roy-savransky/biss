/**
 * @file word_count.c
 * @version 1.0
 * @author ????????
 *
 * @brief program that print how many words are in a given file.
 *
 * @section LICENSE
 * This program is part of the Black-Biss;
 * ANY USE OF THE PROGRAM OR THE SOURCE CODE WITHOUT THE BLACK-BISS AGREEMENT IS FORBIDDEN.
 *
 * @section DESCRIPTION
 * The system get file path as argument. Open it and count the word's in it.
 * Input  : file_path
 * Output : print out the amount of words.
 */


 // ------------------------------ includes ------------------------------
#include <stdio.h>
#include <stdlib.h>

/*a function that count the amount of words in a file*/
int count(char* path)
{
	int c;
	int is_letter = 1; // one for true, zero for false
	int counter = 0;
	FILE* file = fopen(path, "r");
	if (file != NULL) {
		while ((c = getc(file)) != EOF)
		{
			switch (is_letter)
			{
			case 1:
				if (c < 'A' || c>'z' && c!='\'')
				{
					counter++;
					is_letter = 0;
				}
				break;
			case 0:
				if (c >= 'A' && c <= 'z' || c == '\'')
				{
					is_letter = 1;
				}
				break;
			default:
				break;
			}
		}
	}

	return counter;
}

int main(int argc, char* argv[])
{
	size_t word_count = 0;
	// you can add more initial checks
	/*if (argc < 2)
	{
		printf("missing file_path parameter.\n");
		return EXIT_FAILURE;
	}
	*/
	word_count = count("C:\\Users\\office\\Desktop\\work\\biss\\aladdin.txt");
	// open the file and count how many word are in it.

	printf("The file %s contain %u words.\n", "aladdin", word_count);

	return 0;
}