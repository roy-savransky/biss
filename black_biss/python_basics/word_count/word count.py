import sys

def word_dict():
    file_a = open(r"C:\Users\office\Desktop\matzov\roy-savransky-proj\alice_in_wonderland.txt","r")
    lines_a = file_a.read().splitlines()
    word_count = {}
    for line in lines_a:
        if line == "":
            pass
        else:
            new_line = ""
            line = line.strip()
            for i in range(len(line)):
                if i> 0 and line[i] == '-' and line[i-1] == '-':
                    pass
                elif line[i] == '-':
                    new_line += ' '
                elif line[i] in (',', '\'', '"','\\', '!', '_', '-', '*', '`', '~', '?', '/', ':', ')', '(', '%', '#', '@', '>', '<', '[', ']', '{', '}','.', ';', ''):
                    pass
                elif i> 0 and line[i] == ' ' and line[i-1] == ' ':
                    pass
                else:
                    new_line += line[i]
            new_line = new_line.lower()
            words = new_line.split(' ')
            for word in words:
                if word not in word_count.keys():
                    word_count[word] = 1
                else:
                    word_count[word] = word_count[word]+1
    return word_count


def print_words(filename):
    word_count = word_dict()
    keys = []
    for key in word_count.keys():
        keys.append(key)
    keys.sort()
    for key in keys:
        print ("%s : %s" % (key, word_count[key]))

def print_top(filename):
    word_count = word_dict()
    biggest_keys_list = []
    for i in range(20):
        biggest_key = " "
        for key in word_count:
            if biggest_key == " ":
                biggest_key = key
            elif word_count[key] > word_count[biggest_key]:
                biggest_key = key
        biggest_keys_list.append((biggest_key,word_count[biggest_key]))
        del word_count[biggest_key]
    for key, value in biggest_keys_list:
        print ("%s : %s" % (key, value))

def main():
    if len(sys.argv) != 3:
        print("usage: ./wordcount.py {--count | ---topcount} file")
        sys.exit(1)

    option = sys.argv[1]
    filename = sys.argv[2]
    if option == '--count':
        print_words(filename)
    elif option == '--topcount':
        print_top(filename)

    else:
        print("unknown option: " + option)
        sys.exit(1)


if __name__ == '__main__':
    main()




