"""
Black Biss - Basic Python

Write a function that remove all the odd values in  an array
"""


def odd_ones_out(in_array):
    i = 0
    while i < len(in_array):
        if in_array[i] % 2 == 1:
            in_array.remove(in_array[i])
            i -= 1
        
        i += 1
    return in_array




# small test to check it's work.
if __name__ == '__main__':
    arr_base = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
    arr_expected = [0, 2, 4, 6, 8]

    if odd_ones_out(arr_base) == arr_expected:
        print("HAZA! simple test pass")
    else:
        print("Oops, it's not working yet")
